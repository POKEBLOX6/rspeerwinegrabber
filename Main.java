package org.ianfights;

import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.runetek.api.component.tab.*;

import java.util.Random;


@ScriptMeta(version = 5.00, desc = "Telegrabs Wine of Zamoak", developer = "ianfights", name = "Telegrab Wine of Zamorak")
public class Main extends Script {


    private final Spell Grab = Spell.Modern.TELEKINETIC_GRAB;
    private static final Area temple = Area.rectangular(2949, 3519, 2929, 3512, 0);
    private Area BANK_AREA = Area.rectangular(2943, 3373, 2949, 3366, 0);

    private void bank() {
        if (Bank.isOpen()) {
            Bank.depositInventory();
        } else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a -> true);
            }
        }
    }


    @Override
    public int loop() {
        Player local = Players.getLocal();
        final Pickable wine = Pickables.getNearest("Wine of zamorak");
        if (Inventory.isEmpty()) {
            if (!BANK_AREA.contains(local)) {
                Movement.setWalkFlag(BANK_AREA.getCenter());

            } else if (Bank.isOpen()) {
                Bank.withdrawAll("Law rune");
                Bank.withdraw("Staff of air", 1);
                Inventory.getFirst("Staff of air").interact("Equip");
            } else {
                final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
                if (Bank_Booth != null) {
                    Bank_Booth.interact(a -> true);
                }
            }

        }
        if (!Inventory.isFull()) {
            if (!temple.contains(local)) {
                Movement.walkTo(temple.getCenter());
            } else if (wine != null) {
                Magic.cast(Grab);
                wine.interact("Cast");
            }
        }
        if (Inventory.isFull()) {
            if (!BANK_AREA.contains(local)) {
                Movement.walkTo(BANK_AREA.getCenter());
            } if(BANK_AREA.contains(local)) {
                bank();
            }
        }
        return(600);
    }

}


